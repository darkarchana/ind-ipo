import moment from 'moment'

const getIdxIpoCompact = ({Result}) => {
  return Result.map( ipo => {
    return {
      code: ipo.KodeEmiten,
      name: ipo.NamaEmiten,
      listingDate: moment(ipo.TanggalPencatatan).format("dddd, MMMM Do YYYY"),
      ipoSharesValue: ipo.SahamIPOValue
    }
  }) 
}

const response = async(req, res) => {
  const year = new Date().getFullYear()
  const data = await fetch(`
    https://www.idx.co.id/umbraco/Surface/ListingActivity/GetIpoRelisting?
      emitenType=s&
      year=${year}&
      status=ipo&
      indexFrom=0&
      pageSize=100&
      firstChar=`
  )
  const json = await data.json()
  res.statusCode = 200
  res.json(getIdxIpoCompact(json))
}

module.exports = (req, res) => {
  response(req, res)
};